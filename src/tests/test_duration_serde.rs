use std::time::Duration;

use serde::{Deserialize, Serialize};

use crate::serde::duration;

#[derive(Debug, Deserialize, Serialize)]
pub struct Schedule {
    #[serde(with = "duration")]
    pub period: Duration,
}

#[test]
pub fn test_deserialize() {
    let s = "{\"period\":\"01:10:30\"}";
    let v: Schedule = serde_json::from_str(s).unwrap();
    assert_eq!(v.period, Duration::from_secs(3600 * 1 + 60 * 10 + 30));
}

#[test]
pub fn test_serialize() {
    let o = Schedule {
        period: Duration::from_secs(3600 * 1 + 60 * 8 + 20),
    };
    let s = serde_json::to_string(&o).unwrap();
    assert_eq!(s, "{\"period\":\"01:08:20\"}")
}
