use std::collections::VecDeque;

use crate::simcom::{
    extract_many_mqtt_receive_message_lines, extract_mqtt_receive_message_lines,
    parse_mqtt_messages,
};
use int_enum::IntEnum;

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Default)]
pub enum WifiMqttState {
    #[default]
    Unconnected,
    Connected,
    Subscribed,
    Error,
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, Default, PartialEq, PartialOrd, IntEnum)]
pub enum GsmMqttState {
    // GSM modem is not ready for Internet services.
    #[default]
    Off = 0,
    // Powered on, but not ready for MQTT yet
    On = 1,
    // Has seen "PB_DONE"
    Started = 2,
    // Can publish, subscribe (already start MQTT service in modem).
    Ready = 3,
    // Has subscribed, can check for received messages.
    Subscribed = 4,
}

#[test]
pub fn test_compare_enum() {
    assert!(WifiMqttState::Subscribed < WifiMqttState::Error);
    assert!(WifiMqttState::Connected >= WifiMqttState::Connected);
    let a = GsmMqttState::On;
    let b = GsmMqttState::Ready;
    assert!(a <= b);
}

#[test]
pub fn test_parse_mqtt_messages() {
    let raw = "+CMQTTRXSTART: 0,21,1
+CMQTTRXTOPIC: 0,21
demox/snac/mtr00001/7
+CMQTTRXPAYLOAD: 0,1
1
+CMQTTRXEND: 0

+CMQTTRXSTART: 0,21,1
+CMQTTRXTOPIC: 0,21
demox/snac/mtr00001/6
+CMQTTRXPAYLOAD: 0,1
1
+CMQTTRXEND: 0";
    let result = parse_mqtt_messages(raw.trim());
    assert!(result.is_ok());
    let messages = result.unwrap();
    assert_eq!(messages.len(), 2);
    assert_eq!(messages[0].0, "demox/snac/mtr00001/7");
    assert_eq!(messages[1].0, "demox/snac/mtr00001/6");
}

#[test]
pub fn test_extract_mqtt_receive_message_lines() {
    let queue = vec![
        "RUBBISH\r\n",
        "+CMQTTRXSTART: 0,20,1\r\n",
        "+CMQTTRXTOPIC: 0,20\r\n",
        "demox/snac/mtr0000/7\r\n",
        "+CMQTTRXPAYLOAD: 0,1\r\n",
        "1\r\n",
        "+CMQTTRXEND: 0\r\n",
        "+CMQTTRXSTART: 0,21,1\r\n",
        "+CMQTTRXTOPIC: 0,21\r\n",
        "demox/snac/mtr00001/6\r\n",
        "+CMQTTRXPAYLOAD: 0,1\r\n",
        "1\r\n",
        "+CMQTTRXEND: 0\r\n",
    ];
    let mut messages = VecDeque::from_iter(queue.into_iter().map(|s| s.to_string()));
    let extracted = extract_mqtt_receive_message_lines(&mut messages);
    assert!(extracted.is_some());
    let extracted = extracted.unwrap();
    assert_eq!(extracted.len(), 6);
}

#[test]
pub fn test_extract_many_mqtt_receive_message_lines() {
    let queue = vec![
        "+CMQTTRXSTART: 0,19,1\r\n",
        "+CMQTTRXTOPIC: 0,19\r\n",
        "RUBBISH\r\n",
        "+CMQTTRXSTART: 0,20,1\r\n",
        "+CMQTTRXTOPIC: 0,20\r\n",
        "demox/snac/mtr0000/7\r\n",
        "+CMQTTRXPAYLOAD: 0,1\r\n",
        "1\r\n",
        "+CMQTTRXEND: 0\r\n",
        "+CMQTTRXSTART: 0,21,1\r\n",
        "+CMQTTRXTOPIC: 0,21\r\n",
        "demox/snac/mtr00001/6\r\n",
        "+CMQTTRXPAYLOAD: 0,1\r\n",
        "1\r\n",
        "+CMQTTRXEND: 0\r\n",
    ];
    let mut messages = VecDeque::from_iter(queue.into_iter().map(|s| s.to_string()));
    let extracted = extract_many_mqtt_receive_message_lines(&mut messages);
    assert!(extracted.is_some());
    let extracted = extracted.unwrap();
    assert_eq!(extracted.len(), 2);
    assert_eq!(extracted[0].len(), 6);
    assert_eq!(extracted[0][0], "+CMQTTRXSTART: 0,20,1\r\n");
    assert_eq!(extracted[1][0], "+CMQTTRXSTART: 0,21,1\r\n");
}
