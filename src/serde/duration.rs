//! Custom AgriConnect serialization of Duration type, used in RESTful API.

use core::time::Duration;
use std::fmt;
use std::num::ParseIntError;

use serde::{
    de::{Deserializer, Error, Unexpected, Visitor},
    Serialize, Serializer,
};

struct DurationVisitor;

impl<'de> Visitor<'de> for DurationVisitor {
    type Value = Duration;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a string in form of hh:mm:ss")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: Error,
    {
        let parts: Result<Vec<u64>, ParseIntError> = v.split(':').map(|s| s.parse()).collect();
        let parts = parts.map_err(|_| {
            E::invalid_value(Unexpected::Str(&v), &"a string in form of hh::mm::ss")
        })?;
        let parts = TryInto::<[u64; 3]>::try_into(parts).map_err(|_| {
            E::invalid_value(Unexpected::Str(&v), &"a string in form of hh::mm::ss")
        })?;
        let [h, m, s] = parts;
        Ok(Duration::from_secs(60 * 60 * h + 60 * m + s))
    }

    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: Error,
    {
        self.visit_str(&v)
    }
}

/// Deserialize "01:30:40" to "1 hour 30 minutes 40 seconds"
pub fn deserialize<'a, D: Deserializer<'a>>(deserializer: D) -> Result<Duration, D::Error> {
    deserializer.deserialize_str(DurationVisitor)
}

pub fn serialize<S: Serializer>(d: &Duration, serializer: S) -> Result<S::Ok, S::Error> {
    let total = d.as_secs();
    let (h, remaining) = (total / 3600, total % 3600);
    let (m, s) = (remaining / 60, remaining % 60);
    format!("{h:02}:{m:02}:{s:02}").serialize(serializer)
}
