//! Utilities to parse SIMCOM GSM modem responses.

use std::collections::VecDeque;

use log::info;
use nom::bytes::complete::{is_a, tag, take, take_while};
use nom::character::complete::{char, digit1, line_ending, one_of, u16 as to_u16, u8 as to_u8};
use nom::combinator::map_res;
use nom::error::Error as NmError;
use nom::multi::{many1, separated_list0};
use nom::sequence::{delimited, terminated, tuple};
use nom::Finish;
use nom::IResult;

pub fn is_space(c: char) -> bool {
    c.is_whitespace()
}

pub fn zero_or_one(input: &str) -> IResult<&str, u8> {
    let parse = one_of("01");
    let mut xparse = map_res(parse, |s| String::from(s).parse::<u8>());
    xparse(input)
}

fn mark_start(input: &str) -> IResult<&str, &str> {
    let mut parser = terminated(
        tag("+CMQTTRXSTART"),
        tuple((
            char(':'),
            take_while(is_space),
            zero_or_one,
            char(','),
            digit1,
            char(','),
            digit1,
            line_ending,
        )),
    );
    parser(input)
}

fn mark_end(input: &str) -> IResult<&str, &str> {
    let mut parser = terminated(
        tag("+CMQTTRXEND"),
        tuple((char(':'), take_while(is_space), zero_or_one)),
    );
    parser(input)
}

fn parse_topic_length(input: &str) -> IResult<&str, u8> {
    let start_with = tuple((tag("+CMQTTRXTOPIC:"), is_a(" "), zero_or_one, char(',')));
    let mut parse = delimited(start_with, to_u8, line_ending);
    parse(input)
}

fn retrieve_topic(input: &str, length: u8) -> IResult<&str, &str> {
    let mut parse = terminated(take(length as usize), line_ending);
    parse(input)
}

fn parse_payload_length(input: &str) -> IResult<&str, u16> {
    let start_with = tuple((tag("+CMQTTRXPAYLOAD:"), is_a(" "), zero_or_one, char(',')));
    let mut parse = delimited(start_with, to_u16, line_ending);
    parse(input)
}

fn retrieve_payload(input: &str, length: u16) -> IResult<&str, &str> {
    // TODO: Need more testing data to see how the payload in GSM modem response is.
    let mut parse = terminated(take(length as usize), line_ending);
    parse(input)
}

pub fn do_parse_mqtt_message_with_nom(input: &str) -> IResult<&str, (&str, &str)> {
    // Example raw message:
    // +CMQTTRXSTART: 0,20,1
    // +CMQTTRXTOPIC: 0,20
    // demo/snac/mtr00000/1
    // +CMQTTRXPAYLOAD: 0,1
    // 1
    // +CMQTTRXEND: 0
    let (remaining, _) = mark_start(input)?;
    let (remaining, topic_length) = parse_topic_length(remaining)?;
    let (remaining, topic) = retrieve_topic(remaining, topic_length)?;
    let (remaining, payload_length) = parse_payload_length(remaining)?;
    let (remaining, payload) = retrieve_payload(remaining, payload_length)?;
    let (remaining, _) = mark_end(remaining)?;
    Ok((remaining, (topic, payload)))
}

/// Parse the GSM modem response for multiple MQTT Received messages.
/// Return a tuple of (topic, payload).
/// The input string must be priorly trimmed.
pub fn parse_mqtt_messages(raw: &str) -> Result<Vec<(&str, &str)>, NmError<&str>> {
    let (_r, messages) =
        separated_list0(many1(line_ending), do_parse_mqtt_message_with_nom)(raw).finish()?;
    Ok(messages)
}

/// Try extracting lines which compose a MQTT Receive message.
/// We only extract one message from the start of the queue.
/// We should call this function many times if suspect that
/// there are more message in the queue.
pub fn extract_mqtt_receive_message_lines(messages: &mut VecDeque<String>) -> Option<Vec<String>> {
    const MQTT_START: &str = "+CMQTTRXSTART:";
    const MQTT_END: &str = "+CMQTTRXEND:";
    let mut iter = messages.iter().enumerate();
    let start_idx = iter.find_map(|(i, m)| {
        if m.starts_with(MQTT_START) {
            Some(i)
        } else {
            None
        }
    });
    if let Some(i) = start_idx {
        info!("Found starting of MQTT Receive message at index {i}");
    }
    let end_idx = start_idx.and_then(|_i| {
        iter.find_map(|(j, m)| {
            if m.starts_with(MQTT_END) {
                Some(j)
            } else {
                None
            }
        })
    });
    if let Some(i) = end_idx {
        info!("Found ending line of MQTT Receive message at index {i}");
    };
    if let Some((s, e)) = start_idx.zip(end_idx) {
        // If there are other staring lines between start_idx and end_idx, shift start_idx to the latest of these middle,
        // because it means that the previous found indices are from incomplete messages.
        let e = e + 1;
        let true_start_idx = messages
            .range((s + 1)..e)
            .enumerate()
            .filter_map(|(i, m)| {
                if m.starts_with(MQTT_START) {
                    Some(i + s + 1)
                } else {
                    None
                }
            })
            .last();
        let s = true_start_idx
            .inspect(|i| info!("Found another starting line at {i}. Use this index instead."))
            .unwrap_or(s);
        info!("To extract lines from {s}..{e} range.");
        let lines: Vec<String> = messages.drain(s..e).collect();
        Some(lines)
    } else {
        None
    }
}

pub fn extract_many_mqtt_receive_message_lines(
    messages: &mut VecDeque<String>,
) -> Option<Vec<Vec<String>>> {
    let mut out: Vec<Vec<String>> = Vec::new();
    loop {
        let message = extract_mqtt_receive_message_lines(messages);
        if let Some(m) = message {
            out.push(m);
        } else {
            break;
        }
    }
    if out.is_empty() {
        None
    } else {
        Some(out)
    }
}
