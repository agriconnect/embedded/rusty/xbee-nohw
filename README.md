# xBee non-hardware utilities

This crate contains types and utilities which are independent from ESP-IDF.

It is not inside the [xBees] repo because:

- We want to run test on host PC.
- Host PC is not a ESP32, hence cannot build this crate.

[xBees]: https://gitlab.com/agriconnect/embedded/rusty/xbees
